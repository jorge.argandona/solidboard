﻿using SolidBoard.Boards;
using SolidBoard.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Matches
{
    public class Match
    {
        public IBoard board { get; set; }
        public List<IRule> lstRule { get; set; }
    }
}
