﻿using SolidBoard.Boards;
using SolidBoard.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Matches
{
    public class TickTackToe : AMatchBuilder
    {
        public override void BuildBoard()
        {
            this.MyMatch.board = new BoardThree();
            this.MyMatch.board.Create();
        }

        public override void BuildRules()
        {
            this.MyMatch.lstRule = new List<Rules.IRule>();
            this.MyMatch.lstRule.Add(new Diagonal());
            this.MyMatch.lstRule.Add(new Horizontal());
            this.MyMatch.lstRule.Add(new Vertical());
        }
    }
}
