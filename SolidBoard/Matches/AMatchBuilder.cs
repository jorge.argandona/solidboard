﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Matches
{
    public abstract class AMatchBuilder
    {
        public Match MyMatch { get; set; }

        public abstract void BuildBoard();
        public abstract void BuildRules();
    }
}
