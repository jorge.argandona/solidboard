﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Matches
{
    public class MatchManager
    {
        public void ConstructMatch(AMatchBuilder match)
        {
            match.BuildBoard();
            match.BuildRules();
        }
    }
}
