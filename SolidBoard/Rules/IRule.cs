﻿using SolidBoard.Boards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Rules
{
    public interface IRule
    {
        bool Verify(IBoard board);
    }
}
