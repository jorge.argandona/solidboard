﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolidBoard.Boards;
using SolidBoard.Matches;

namespace SolidBoard.Rules
{
    public class Horizontal : IRule
    {
        public bool Verify(IBoard board)
        {
            bool resp = false;
            List<Position> columns;
            Player firstPlayer;
            Player nextPlayer;

            for (int i = 1; i <= board.Rows; i++)
            {
                columns = board.Positions.Where(p => p.Row == i).ToList();
                firstPlayer = (0 < columns.Count) ? columns[0].Mark : null;

                if (firstPlayer == null)
                {
                    continue;
                }

                for (int j = 1; j < columns.Count; j++)
                {
                    nextPlayer = columns[j].Mark;

                    if ((nextPlayer == null) || (nextPlayer.NickName != firstPlayer.NickName))
                    {
                        break;
                    }

                    if (j == (columns.Count -1))
                    {
                        resp = true;
                    }
                }
            }

            return resp;
        }
    }
}
