﻿using SolidBoard.Matches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Boards
{
    public class Position
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public Player Mark { get; set; }

        public Position()
        {
            this.Mark = null;
        }
    }
}
