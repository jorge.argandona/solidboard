﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Boards
{
    public class BoardThree : IBoard
    {
        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public List<Position> Positions { get; private set; }

        public BoardThree()
        {
            this.Positions = new List<Position>();
            this.Rows = 3;
            this.Columns = 3;;
        }

        public void Create()
        {
            for (int i = 1; i <= this.Rows; i++)
            {
                for (int j = 1; j <= this.Columns; j++)
                {
                    this.Positions.Add(new Position {
                        Row = i,
                        Column = j
                    });
                }
            }
        }

    }
}
