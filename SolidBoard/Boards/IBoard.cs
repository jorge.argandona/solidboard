﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard.Boards
{
    public interface IBoard
    {
        List<Position> Positions { get; }

        int Rows { get; }

        int Columns { get; }

        void Create();
    }
}
