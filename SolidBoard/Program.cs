﻿using SolidBoard.Matches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidBoard
{
    class Program
    {
        static void Main(string[] args)
        {
            MatchManager matchManager = new MatchManager();
            AMatchBuilder match = new TickTackToe();

            matchManager.ConstructMatch(match);
            Match myMatch = match.MyMatch;
        }
    }
}
